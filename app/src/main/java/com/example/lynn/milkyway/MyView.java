package com.example.lynn.milkyway;

import android.content.Context;
import android.graphics.Color;
import android.widget.RelativeLayout;

import static com.example.lynn.milkyway.MainActivity.*;

/**
 * Created by lynn on 6/19/2015.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        setBackgroundColor(Color.BLACK);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.leftMargin = center.x;
        layoutParams.topMargin = center.y;

        Planet sun = new Planet(context,0xFFFFFF00);

        sun.setLayoutParams(layoutParams);

        addView(sun);

        Planet mercury = new Planet(context,0xFFFF0000);

        addView(mercury);

        new PlanetThread(mercury,40);

        Planet venus = new Planet(context,0xFF0000FF);

        addView(venus);

        new PlanetThread(venus,80);

        Planet earth = new Planet(context,0xFF00FF00);

        addView(earth);

        new PlanetThread(earth,120);

        Planet mars = new Planet(context,0xFFFF0000);

        addView(mars);

        new PlanetThread(mars,160);

        Planet jupiter = new Planet(context,0xFF0000FF);

        addView(jupiter);

        new PlanetThread(jupiter,200);

        Planet saturn = new Planet(context,0xFFFFFF00);

        addView(saturn);

        new PlanetThread(saturn,240);

        Planet uranus = new Planet(context,0xFF0000FF);

        addView(uranus);

        new PlanetThread(uranus,280);

        Planet neptune = new Planet(context,0xFF00FF00);

        addView(neptune);

        new PlanetThread(neptune,320);






    }

}

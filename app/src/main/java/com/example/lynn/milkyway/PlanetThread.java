package com.example.lynn.milkyway;

import android.widget.RelativeLayout;

import static com.example.lynn.milkyway.MainActivity.*;

/**
 * Created by lynn on 6/29/2015.
 */
public class PlanetThread implements Runnable {
    private Planet planet;
    private int distanceFromSun;
    private boolean keepGoing;

    public PlanetThread(Planet planet,
                        int distanceFromSun) {
        this.planet = planet;

        this.distanceFromSun = distanceFromSun;

        keepGoing = true;

        new Thread(this).start();
    }

    private void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    @Override
    public void run() {
        while (keepGoing) {
            for (int angle=0;angle<360;angle+=20) {
                final int x = center.x + (int)(distanceFromSun*Math.cos(angle*3.1415926535/180));
                final int y = center.y + (int)(distanceFromSun*Math.sin(angle*3.1415926535/180));

                planet.post(new Runnable(){
                    @Override
                    public void run() {
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) planet.getLayoutParams();

                        layoutParams.leftMargin = x;
                        layoutParams.topMargin = y;

                        planet.setLayoutParams(layoutParams);

                    }
                });


                pause(0.5);


            }


        }

    }

}

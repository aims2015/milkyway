package com.example.lynn.milkyway;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by lynn on 6/19/2015.
 */

public class Util {
    public static ImageView background;
    public static ImageView[] views;
    public static Drawable[] drawables;
    public static int[] ids;

    public static Point getCenter(Context context) {
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();

        Point output = new Point();

        display.getSize(output);

        output.x /= 2;
        output.y /= 2;

        return(output);
    }
}

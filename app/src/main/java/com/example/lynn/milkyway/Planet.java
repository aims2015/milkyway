package com.example.lynn.milkyway;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by lynn on 6/29/2015.
 */
public class Planet extends View {
    private Paint paint;

    public Planet(Context context, int color) {
        super(context);

        paint = new Paint();

        paint.setColor(color);
    }

    public void onDraw(Canvas canvas) {
        canvas.drawCircle(10,10,5,paint);
    }


}
